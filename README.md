# Description

This project contains all the notes that I am creating for GEOG-254. Anyone
is encouraged to create pull requests to change anything wrong or clone the
repository for their own private viewing

# Structure

The notes are taken in markdown and then compiled into pdf format with pandoc.
Depending on the need I'll embed some latex here and there. So if all you want
to look at is the finished product of the notes go to the releases page and look
for `master.pdf` that will have all my notes for the semester. If you want to
look at the source go through the directory for each topic and find the markdown
files

## Directory Structure

The repo makes heavy use of directories for organization. The relevant notes for
each topic will be contained in a directory titled with just the topic number

E.g. `1/` will contain the notes for all of topic 1

# Compilation

For anyone wishing to contribute or just to build from source here's the script
I use

`./PandocNotes.sh`

This will recursively go through every topic's directory and compile the source
(markdown) documents into `master.pdf` using the settings from `pandoc.yaml`.
Documents will be put into `master.pdf` in alphanumeric order
