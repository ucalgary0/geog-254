# Topic 2 Lecture Notes

> What is Human Geography?
> 02/24/2022

## Aspects of Place

- Changing attributes of place
  - Places change over time
    - How did this place / thing come to be what / where it is?
    - How has this place been changed over time by humans?
  - Influences can be physical, cultural, architectural, linguistic, or colonial
- Interrelations between places
  - Spatial interaction (accessibility and connectivity)
  - Spatial diffusion 
  - Globalization
- Rational structure of place
  - Density
    - Why places tend to be in one location
  - Dispersion
  - Pattern

### Spatial Diffusion

Spatial Diffusion:
: Dispersion of idea to distant points that are (in)directly connected

- Ideas can be either + / -
  - Technology, language, thought, religion are +
  - Terrorism, disease is -

### Globalization

Globalization:
: Increased connection of peoples + societies in all parts of the world
: - Also means other things to many people (+ / -)

### Scared Places

Scared places can also change in meaning over time

### Physical & Cultural Attributes

Natural Landscape:
: Physical characteristics of the land that inhabited
: - Climate, soil, topography, vegetation
: - As humans manipulate the physical landscape, maintaining that landscape
requires their presence

Cultural Landscape: 
: Footprint we leave on the landscape

## Five Key Themes of Human Geography

- Location
  - Where places are
  - How they change
- Place
  - How places gain identity
- Region
  - How areas of the Earth can have defined attributes
- Movement
  - Spatial diffusion
- Human / Environment Interaction

## Types of Human Geography

Geography has many subdisciplines

![Geography Wheel](~~~/2/geography-wheel.png)

### Cultural Geographies

- Cultural Processes
  - Cultural Production
    - Politics, how culture is produced
  - Cultural Consumption
    - How culture is taken up, interpreted, or commodified
- Cultural Geographies
  - Buildings, text, landscapes, media, design
- Space and Place
  - Space not defined, place is defined
  - Leads into concepts of sense of place and identity

### Social Geographies

Social Geography:
: Applying social theory to spatial concepts, distribution, and relationships

- Concepts of ...
  - Ethnicity
  - Conceptualization of race
  - Socioeconomic inequality
- Patterns & distribution of social groups and how they relate to one another
  - How social processes take place across space
  - How these processes take place in urban environments
- Closely related to concepts within Sociology but more focused on spatial
  considerations

### Urban Geography

Urban Geography:
: Applies other disciplines (sociology, anthropology, history, political
science, geography) to an urban environment

- Queens & Manhattan on opposite ends of same city yet vastly different
  - Both become valuable in different ways
- Where do people go when an area is gentrified?
- How do people build "utopias" considering resources?
  - Where do lower income people live?
- How rapid urbanization / growth takes place related to economic forces

### Political Geography

- Processes as old as human history
  - Tribes + clans
  - Empires + kingdoms
  - Modern states
- Human groups have laid claim to territory
  - Organized and administered affairs within defined space
  - Held territory through negotiation / means of force
  
Political geography is concerned with creation & keeping of boundaries
(also called morphological classification of boundaries)

- Natural boundaries
- Geometric boundaries
- Historical
  - Antecedent
    - Defined before people occupied / defined it
  - Subsequent (ethnographic)
    - Defined relating to how people have migrated / held territory through
      tribes / religion
  - Subsequent (superimposed)
    - Don't represent ethnographic boundaries and often create conflict
  - Relict
    - Existed at one time but still have cultural significance
- Other

### Population Geography

Population Geography:
: Looks at demographics, space, and population growth relating to resources

- World population increased from 3B-6B from 1959-1999
  - Doubling occurred over 40 years (100% increase)
  - Population growth will continue at a slower rate into 21st century
    - Growth from 6B-9B from 1999-2042 (50% increase)
  
### Economic Geography

Economic Geography:
: Study geographically specific factors that shape economic process & identify
key agents (firms, labour, the state)
: Study effects of globalization, causes / consequences of uneven development

### Development Geography

Development Geography:
: Linked to economy while considering other aspects

- "Other aspects" may include...
  - Income disparity
  - % of required dietary energy received daily
  - % access to safe drinking water
  - Infant mortality rate
  - Physicians per capita
  - Gender Empowerment Measure
    - % females in positions of political power
    - % females in professional & technical jobs
    - % of females in administrative & managerial positions
