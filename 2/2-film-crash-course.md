# Crash Course Geography #28

> What is Human Geography?
> 02/24/2022

Cultural Geography:
: Examines how markers of identities are visible across space

Political Geography:
: Examines the way power shapes the landscape

Economic Geography:
: Examines the uneven movement of economic opportunity

Urban Geography:
: Examines the way humans build cities

Human geography uses a combination of spatial, physical, and social science to
tell a complex story of the world

- Humans & environment are always interacting
  - Create rich cultures, navigate economic systems, organize ourself through
    politics
  - We like to build physically + metaphorically
    - A lot of what we build comes from / impacts the environments around us

Names let us talk about things, share ideas, and build identities. Geographers
naturally learn names as they learn about how social and physical systems
interact together

- Learning about economic, technology, language, religion, power, and how they
  all move lets us tell fuller richer stories of the Earth

Scale:
: The relationship a place has to the rest of the world

Toponym:
: A place's name derived from a topographical feature

- Studying names is an important part of cultural geography
  - History of a toponym mirrors the history of the location
  - Examines how markers of identity are visible across space
    - Names, language, religion, art, etc.
- Names can indicate power
  - Denali became Mt. McKinley because of the US' power
  - Power is distributed across space in part by the toponyms we give places.
- A name sets up the way we imagine the space
  - Who owns it or what the culture should be
  - Europeans tried to rename places to erase culture and prior impact
  
Region:
: Way of grouping and classifying similar places
: - Administrative regions, regions w/ legal boundaries, etc.

- Renaming of Denali signifies a new perceptual region
  - Created a relationship between Ohio and Alaska
- Denali had its name reinstated in 2015
  - Reflected people who had ancestral connection to land
  - Represents growing movement of marginalized groups asserting their cultural
    understanding of the places they have spiritual, cultural, legal rights to
    
Many maps / borders represent modern geopolitical divisions that have been
decided without the consultation, permission, or recognition of the land's
original inhabitants

- Many geopolitical place names don't reflect the Indigenous / Aboriginal
  peoples' languages
