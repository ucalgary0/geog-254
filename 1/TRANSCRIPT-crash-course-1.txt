 George Eliot’s Old Grandfather Rode A Pig Home Yesterday. Mnemonics are great
 for trying to remember our 4th grade spelling words. But actually deciding what
 that word geography means is a bit trickier. Sure, we memorize state and world
 capitals [because everyone’s impressed if you can rattle them off - like the
 capital of Canada? Ottawa.] Or we learn that rivers flow downhill or that the
 US imports more than 3 billion pounds of bananas from Guatemala each year. And
 those are cool factoids, but that’s not all Geography is. Geography helps us
 answer bigger questions like "what's the story of the Earth?", "how do humans
 change their environments?", and "why, of all places, did that huge mountain
 form there?" There's a lot to cover in this series because geography
 encompasses all 4.5 billion years or so of the Earth's history and even makes
 predictions about our future. So we’re going to do our best to highlight the
 weirdest, most awe-inspiring parts. I’m Alizé Carrère and welcome to Crash
 Course Geography. Let’s take a closer look at that last factoid I threw out
 and... go a little bananas. It might come in handy at a trivia game to know the
 volume of the US-Guatemala banana trade, but there must be more to the story.
 Like, why Guatemala? And why bananas? In geography, we use those questions to
 better understand the connections between us and the physical world. So today,
 let’s start in Guatemala. We think of the land now called Guatemala as part of
 Central America, a region between North and South America that's covered with
 dense rainforests and incredible biodiversity. To look just at Guatemala, we’d
 jump between 13°45’ and 17°48’ north latitude and 88°14’ and 92°13’ west
 longitude. We could even give the absolute location, or geographic coordinates,
 of different geographic points of interest in Guatemala, from Volcan de Fuego
 to the Mayan ruins at Tikal. From there we might notice the physical
 environment -- like the climate, the landforms, or the rivers and waterways.
 Guatemala is a mountainous country with both recently active and long dormant
 volcanoes that have provided rich, fertile soil. With soil like this, it seems
 like there’s no shortage of options for what would grow, and yet...bananas. It
 turns out, to be successful, bananas need to grow at a temperature between 20
 and 35 degrees celsius -- Guatemala’s tropical regions range between 18 and 35
 degrees. Bananas need about 170 centimeters of rain a year -- most of Guatemala
 gets between 70 and 200 centimeters. And bananas need well-drained soils rich i
 n potassium -- Guatemala's volcanoes spew rocks rich in iron, magnesium, and --
 you guessed it -- potassium. What we’re doing here is identifying the space, or
 the features and relationships that occur in a given area. Basically it’s the
 cold, hard facts about a specific location on Earth’s surface. We need to
 pinpoint where we’re interested in, before we can start to answer why various
 things happen there. Working with the idea of space is one of the defining
 characteristics of geography, and we’ll get into even more specifics in later
 episodes. Historically, maps, and more recently, satellite images are tools
 that help define and quantify space. But there are plenty of non-spatial things
 we might already have in our minds about Guatemala and Central America. For
 example, that it’s long been home to large populations of indigenous peoples
 including Mayan groups like the K’iche’, Kaqchikel, and Mam, and non-Mayan
 groups like the Xinca. Or that it’s a region known for its history of empires
 like the Mayan or those created by Spanish colonizers. Guatemala has been known
 by many names including Cuauhtēmallān, a name given to the area by Tlaxcalan
 warriors accompanying Spanish Conquistadors. Like almost any land or mountain
 or stretch of sea, “Guatemala” means different things to different people. It’s
 a place, or somewhere that has attached value, meaning, and emotion to it that
 can’t be measured. It’s subjective, for sure, but a place can be observed and
 described to others. We can think of place as the significance attached to a
 particular space. So as we try to better understand the significance of bananas
 and how they fit into Guatemala, the space and the place, we’d learn bananas
 actually aren’t native to Guatemala or even the Americas. Explorers and
 missionaries brought bananas to Central America in the 1500s from the areas
 near present-day Indonesia and Papua New Guinea where bananas grow natively.
 Thinking about where bananas can grow in Guatemala and why adds another layer
 to our geographical investigation. After all, almost one out of every three
 people in the workforce works in agriculture as of 2020. So the next chapter in
 the “Geography of Guatemalan Banana Imports” story is thinking about
 interactions humans have with the environment. In geography, human-environment
 interactions are all the ways humans connect with and live within the
 environment and the impact the environment has on lives, choices, and
 experiences of people. This is key to geographers. So in Guatemala, where
 there’s enough flat land and fertile soil and it’s not too hot or cold or dry
 or wet, humans might decide to grow bananas. But that still doesn't tell us how
 bananas came to be one of the main crops grown in Guatemala or why there’s so
 much trade in bananas specifically with the US. If we think about demand
 economics, one answer for why the US imports more than 3 billion pounds of
 bananas from Guatemala each year is because there are no tariffs or import
 restrictions, and transportation costs are fairly low. Other banana hotspots
 like Ecuador, Panama, and India are a bit farther away, so transportation is
 more expensive. The greater the ocean distance, the higher the price. But
 that’s not the whole story. To this day, the agriculture industry in Guatemala
 relies on the plantation, which is a large scale commercial enterprise that
 just produces one crop and mostly exports it. Plantations arrived in Guatemala
 with European explorers colonizing the Americas. But they can also be found in
 other parts of the world that experienced colonialism, like cocoa plantations
 in the West Indies, tea plantations in Sri Lanka, and cotton plantations in the
 US. No matter where they’re located, using plantations has had long lasting
 consequences we still contend with today. To peel back the layers, let’s go to
 the Thought Bubble. Bananas first became popular as a “rare and delicious
 treat” in the United States in the late 19th century --even though they’d long
 been a diet staple in many tropical regions. Sensing an opportunity, American
 businessmen like Minor C. Keith and Andrew Preston started importing them from
 around Latin America. The two men were forced to merge their lucrative banana
 empires in 1899. Tropical Trading and Transport Company in Central America
 joined with the Boston Fruit Company that dominated the Caribbean, creating the
 soon-to-be-infamous United Fruit Company. Along with others, it would become so
 powerful that in 1901 the author O. Henry described countries like Honduras and
 Guatemala as “banana republics”-- a reference to the vast control the fruit
 companies wielded over many nations. For example, in 1904, Keith, as vice
 president of United Fruit, signed an exclusive deal with President Manuel
 Estrada Cabrera that gave the company tax-exemptions, land grants, and control
 of all railroads on the Atlantic side of Guatemala. By the 1930s United Fruit
 was the largest landholder in Guatemala. Across Latin America they became
 embroiled in violent disputes, like the 1928 Banana Massacre in Colombia that
 was immortalized in Gabriel Garcia Marquez’s great novel, “100 Years of
 Solitude.” Or the 1934 Great Banana Strike that eventually led to the creation
 of trade unions in Costa Rica. Or in 1954 when they lobbied the US government
 to stage a coup and depose the Guatemalan president when hoarded United Fruit
 land was being redistributed. Which the US did. They had to be politically
 involved to keep control. A US-backed military dictatorship didn’t actually
 help their stock value, but such a big and profitable company had connections
 across the US government and were able to set up agreements that persist in
 some form or other today. Which means that the US still gets most of its
 bananas from Guatemala. Thanks Thought Bubble! It might seem like we’ve
 confused History for Geography, but the sordid past of the banana isn’t in the
 past at all. You might not have heard of United Fruit, but you’ve probably seen
 the label in grocery stores or heard of Chiquita bananas. The United Fruit
 Company eventually became Chiquita Brands International in 1984, which is still
 the number one US supplier of bananas today. Basically, banana plantations have
 had a huge influence on the unequal distribution of land and wealth, leading to
 peasant uprisings, repressive military regimes, and the growing economic
 inequalities in Guatemala. Entire books could be written on the last 150 years
 of banana trade and they’d read like political thrillers. So with fertile soil,
 the political power structure, the rise of colonialism, and Europeans swooping
 in to create plantations...bananas have been stamped into Guatemalan history.
 Wow! All that from just one little factoid about a fruit you can buy in pretty
 much any corner store across the US. I told you that Geography was complicated!
 And every factoid actually has a story behind it. But there are always more
 questions. Like, if we focus on the environment part of human-environment
 interactions, what’s the environmental impact of these large plantations? And
 this is just Guatemala. What about the other places in the world where bananas
 grow – Costa Rica, Panama, and the Dominican Republic. What’s the story
 associated with their banana exports? And that’s why just the factoid you
 learned in 4th grade alone isn’t geography. Geography is that factoid and the
 story that surrounds it. We just looked at the geography of bananas, but we
 could have done the same thing for chocolate. Or the Nile River valley. Or heat
 islands in the Chicago area. The Earth has so many stories, and geography is
 here to tell them! Clearly, the world is complicated. But in geography we try
 to look at the big picture – the confluence of space, place, and the human and
 environment interactions and how they’ve overlapped to bring us this far into
 the story. This is what makes geography a spatial science – it’s all about how
 things vary from place to place and asking “why here?” No two places are the
 same, but when we ask questions to learn more about one place, we just might be
 able to explain what is happening in another place. Of course, geographers are
 going to make mistakes because we’re curious, imperfect, wonderful humans. And
 there will be so many more moments where we go bananas and realize what we
 thought was just a cool fact actually has a huge backstory. There’s a whole
 team working on Crash Course Geography trying hard to avoid making mistakes,
 but we also know that when we tell a story we make certain assumptions, or we
 have to leave out facts to make sure there's a beginning, middle, and end in a
 10 minute video. So as we move through this series and learn together, let’s
 all try to think about the interconnectedness of Earth and its peoples and
 economies and histories. And the fact that a banana factoid can be way more
 complicated than we expect. That’s what will make us all a little more
 thoughtful and geographically aware. So, what is geography? It’s so much more
 than just identifying cities and countries and capitals on a map. Geographers
 look to find connections between the physical processes at work on Earth’s
 surface (and under the surface too) and how people use and interact with the
 Earth. Next time, we’ll look at one of the most useful tools that geographers
 use: maps. Maps tell their own story, and can even be made specifically to tell
 a particular story. Many maps and borders represent modern geopolitical
 divisions that have often been decided without the consultation, permission, or
 recognition of the land's original inhabitants. Many geographical place names
 also don't reflect the Indigenous or Arboriginal peoples languages. So we at
 Crash Course want to acknowledge these peoples’ traditional and ongoing
 relationship with that land and all the physical and human geographical
 elements of it. We encourage you to learn about the history of the place you
 call home through resources like native-lands.ca and by engaging with your
 local Indigenous and Aboriginal nations through the websites and resources they
 provide. Thanks for watching this episode of Crash Course Geography which was
 made with the help of all these nice people. If you would like to help keep all
 Crash Course free for everyone, forever, please consider joining our community
 on Patreon. 
