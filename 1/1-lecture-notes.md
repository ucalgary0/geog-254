# Topic 1 Lecture Notes

> What is Geography?
> 02/23/2022

## Introduction

Geography:
: Interpretation of relationships in space of both physical + human environments
: - An extremely fragmented discipline (split between natural + social
classifications)

Geography is...

- A Description of the Earth
  - Interactions between cultures/countries
  - Natural hazards, climate change, conflict, etc
- Spatial Science
  - Base discipline behind anything that studies space as it relates to each
    other 
  - Many disciplines can be linked together
    - ![Ex. Geography Wheel](~~~/1/geography-wheel.png)
- Study of Spatial Variation
  - How things change over the landscape
  - Socioeconomic differences
  - Landscape and architecture can show a place's history through the influence
    of the people that touched it

### Geography's Niche

- Both a physical and a social science
- Looks at many grey areas between the physical and human environment
  - Pollution
  - Ecosystem
  - Oceans
- Studied from both the human and physical perspective

## Evolution of the Discipline

### Ancient Period

- Eratosthenes coined the term geography
  - Geo (Earth) + graphein (write)
  - Essentially writing about the Earth
- Strabo looked at the inhabited world 
  - How we can map it 
  - How we can understand it
- Herodotus tried to understand the variables of society
  - How society functioned over space
- Ptolemy mapped the known world
- Idrisi was first global representation
  - First globe representation of world

### Non-Western Contributions

- Much knowledge was lost to history
- Maps that Europeans used to traverse new world theorized to come from China

### Modern Period

- Increased in importance over time
  - Was a way to follow people's comings and going
  - Used to judge the usage of a place
  - Gained strategic value in war/politics

## The Geographers' Questions

- Locations have a *sense of place/placelessness*
  - Powerful psychological bond between people and regions
    - *Imagined community*
  - Older a place is the more identity it'll have
    - Identity not easily changed but can be (urban forests)
    - A place can represent numerous identities (San Francisco)
  - Placeless places have no special relationship to their location
    - New Orleans has a sense of place while Canadian Tire doesn't
- Absolute vs relative space
- Spatial behaviour
  - How people move across space
    - How we extract resources
    - How we interact within buildings
  - How things are built/designed/used

### Location

Absolute location:
: Coordinates

Relative location:
: Where something is in relation to something of relevance

Locations are important for geostrategic reasons and resources

Site:
: Physical attributes of location a place occupies

Situation:
: Where a city is in relation to its surrounding features

#### Site vs Situation (New Orleans)

- Site is disadvantageous
  - City lies below sea level
    - Needs special protection from flooding
- Situation is advantageous
  - Oil refineries located here because oil is exported + imported
  - On the mouth of the Mississippi River
    - Good for transporting goods
