# Crash Course Geography #1

> What is Geography?
> 02/23/2022

Questions are used to better understand the connections between us and the
physical world

> Rest of video will use Guatemala as a framing device to introduce key points
> about geography

When describing a location, you might give off its...

- Physical environmnent
  - Climate, landforms, rivers, and waterways
  - *Covered in dense rainforests*
- Absolute location
  - Coordinates
- Space (features/relationships that occur in given area)
  - One of the defining characteristics of geography
  - *Bananas grow in Guatemala because they feed off the potassium spewed by the
    location's volcanos*
- Place (unmeasurable attached value, meaning, emotion)
  - *Guatemala is home to many people*
  - *Guatemala has been known by many names*
  - *Guatemala means different things to different people*
- Interactions between humans and the environment
  - *Bananas are not native to Guatemala, and were brought by explorers +
    missionaries*
- History
  - Geography informs history
  - *Guatemala became such a lucrative place to grow bananas that it became known
    as a "banana republic"*
    - Name in reference to the power fruit companies wielded over the region
    - *Banana plantations had a huge influence on the unequal distribution of
      land and wealth, leading to peason uprisings, repressive military regimes,
      and growing economix inequalities*
      
Geography is a spatial science because it looks at the confluence of space,
place, and the human and environment interactions and how they overlap

Geography looks to find connections between the physical processes at work on
Earth's surface and how people use and interact with the Earth
