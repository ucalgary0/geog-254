# Topic 3 Lecture Notes

> Intro to Population Geography
> 02/24/2022

## Introduction to Population Geography

Population Geography
: Examines how experience, demographics, characteristics take place across space
and time

Demography:
: Study of characteristics of human population and distribution

Overpopulation:
: Judgement to when environment unable to support given population

(Human) Carrying Capacity:
: \# of people an area can support on sustained basis given prevailing tech

## Population Growth

- World population confined to < 30% of Earth's surface
  - Desirable places / places that can sustain populations
- Population grew slowly for 1000s of years, then grew rapidly in 19/20th
  century
  - World population increased from 3B-6B from 1959-1999
    - Doubling occurred over 40 years (100% increase)
    - Population growth will continue at a slower rate into 21st century
  - Population growth slowing / declining in some parts of world
  - Projected population by 2050 9.7, by 2100 11B (from 7.8B by 2020)
- Huge rises in population attributed to increase in technological ability
  - Industrial Revolution & Agrarian Revolution

![Projected world population until 2100](~~~/3/projected-population.png)

World population is heavily clustered

![Major population clusters](~~~/3/population-clusters.png)

- Changes in population growth not always steady
  - Dip due to Great Leap Forward in China
  - Natural disasters decrease agricultural output
  - Massive social reorganization caused China's death rate to rise sharply &
    fertility rate to fall by half
- If human carrying capacity is passed, may not effect everyone equally
  - Some may continue to prosper

### Population Trends in 21st Century

- \> 50% population growth 2020-2050 in Africa (projected)
  - Sub-Saharan Africa's population to double
- 61% (4.7B) of global population lives in Asia
  - India (1.39B) and China (1.44B) represent 37% of world population
- Population of Europe is shrinking
  - Many countries expected to see decline of \> 15%
- Japan life expectancy to rise 72.6-77.1 from 2019-2050

### Population Trends since 1950's

- Growth rate increased 1.5% / year from 1950-1951
  - Peaked \> 2% in 2960s due to reduction in mortality
- Average births / woman 6-2.24 (1960-2017)
  - Concentrated effort made to reduce birth rate
    - Mostly done through education
    - More egalitarian distribution of wealth also helps
- Growth rates then started to decline
  - Rising age of marriage
  - Availability use of effective contraception

![Growth rate begins to decline](~~~/3/growth-decline.png)

## "The Population Bomb"

Paul Ehrlich -
: Biologist and Neo-Malthusian who wrote "Population Bomb" (1968)

Neo-Malthusianism:
: Belief that population control through use of contraception essential for
survival of Earth's human population

- Predicted diminishing resources (energy, famine) / environmental catastrophe
  - Impact = population * affluence * technology
  - Overconsumption in North + overpopulation in South = disaster
- Neo-Malthusian predictions have not come true
  - "No known physical limits to processes of growth + improvement"
  - Does not mean that caution is unneeded
  - Green Revolution prevented famine yet caused numerous environmental problems
    - Time lag between action & consequence
    - Cumulative effects of environmental disturbance
    - Increase gap between rich and poor
  
| Ecological Worldview               | Expansionist                       |
|------------------------------------|------------------------------------|
| Paul Ehrlich                       | Julian Simon                       |
| Rachel Carson                      |                                    |
| Man must respect balance of nature | Man is steadily controlling nature |
| Suspicion of "techno-fix"          | Faith in tech, economic growth     |
| [Worldview] Change, reform         | Status quo                         |

Both viewpoints have valid points

### Population Control

The concept of population control carries many moral dilemmas along with it

- Infringement on individual's rights vs community's rights
  - Do ends justify means? (forced sterilization!!!)
- What moral high ground do we have to tell others to limit their population?
- What world do we leave if we don't limit population growth?

United Nations International Conference on Population and Development (Cairo
1994) abandoned term "population control" & asserted improving position of women

- Priorities instead...
  - Increase education
  - Promoting economic growth / decreasing inequity

## Canada's Population

- Canadians overwhelmingly urban (2014 data)
  - 81.41% live in population centre (\>= 1000 people & 400+ / square km)
  - 69.9% live in large CMAs (\> 300,000)
  - 35.3% live in Toronto, Montreal, Vancouver
  - \> 50% live in suburbs
- Part of larger urbanization across world

![Canada change in population across regions](~~~/3/canada-change.png)

- Change in population motivated more by immigration than increase in births
  - Does not necessarily imply that births are stagnating
- Average Canadian ~41 years old (2019 data)
  - Youngest = Nunavut (26.2)
  - 2nd Youngest = Alberta (37.1)
  - Middle = Western Canada (38.7)
  - Oldest = Newfoundland + Labrador (47.1)

![Canada's age distribution circa 2000](~~~/3/canada-age-distribution-2000.png)

![Canada's age distribution circa 2016](~~~/3/canada-age-distribution.png)

![Projected age distribution of Canada](~~~/3/canada-age-distribution-projection.png)

![Canada proportion of people 65 and older](~~~/3/canada-old-proportion.png)

- Canada has a slightly aged population

### Comparison to Other Countries

![China change in age distribution](~~~/3/china-age-distribution-change.png)

![Comparison between age distributions of Canada & Vietnam](~~~/3/canada-vietnam-age-compare.png)
 
|                       | Canada     | Vietnam    |
|-----------------------|------------|------------|
| Population (2019 est) | 37,411,038 | 96,462,108 |
| Total Area (sq km)    | 9,984,670  | 331,210    |

## Diffusion

Expansion / Contagious Diffusion:
: Importance of direct contact with the developing centre

Hierarchical Diffusion:
: Diffusion from large -\> small areas of importance

Stimulus Diffusion:
: Ideas stimulate development of similar ideas / concepts / technologies

Relocation Diffusion:
: Innovation / idea / population physically carried / forced to new areas by
migrating individuals / populations

Diffusion Barriers:
: Conditions that hinder flow of information / movement of people

Syncretism:
: Process of fusion of old + new
: - Toltecs taking up culture of Mayan civilization in Central America
: - Romans taking up culture + technology of Etruscans / Carthaginian / Greeks
